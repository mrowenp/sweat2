from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils.text import slugify

from .models import ExerciseName


# sends a signal every time a new user is created, and calls this method to create the profile
# @receiver(post_save, sender=ExerciseName)
# def create_profile(sender, instance, created, **kwargs):
#     if created:
#         print('-------------------signals----------------')
#         exercise_name = instance
#         exercise_name.slug = slugify(exercise_name.exercise_name)
#         # profile, new = UserProfile.objects.get_or_create(user=instance)

@receiver(pre_save, sender=ExerciseName)
def exercise_name_populate_slug(sender, instance, **kwargs):
    print('-------------------signals----------------')
    exercise_name = instance
    exercise_name.slug = slugify(exercise_name.exercise_name)