# Instead of views.py the api view code is going here

from rest_framework import generics

from .models import ExerciseName
from .serializers import ExerciseNameSerializer


class SnippetList(generics.ListCreateAPIView):
    queryset = ExerciseName.objects.all()
    serializer_class = ExerciseNameSerializer


class SnippetDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ExerciseName.objects.all()
    serializer_class = ExerciseNameSerializer

#
# @api_view(['GET', 'POST'])
# def exercise_name_list(request, format=None):
#     """
#     List all code exercise_name, or create a new exercise_name.
#     """
#     if request.method == 'GET':
#         exercise_name = ExerciseName.objects.all()
#         serializer = ExerciseNameSerializer(exercise_name, many=True)
#         return Response(serializer.data)
#
#     elif request.method == 'POST':
#         serializer = ExerciseNameSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
#         return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# @api_view(['GET', 'PUT', 'DELETE'])
# def exercise_name_detail(request, pk, format=None):
#     """
#     Retrieve, update or delete a code snippet.
#     """
#     try:
#         exercise_name = ExerciseName.objects.get(pk=pk)
#     except ExerciseName.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#
#     if request.method == 'GET':
#         serializer = ExerciseNameSerializer(exercise_name)
#         return Response(serializer.data)
#
#     elif request.method == 'PUT':
#         data = JSONParser().parse(request)
#         serializer = ExerciseNameSerializer(exercise_name, data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     elif request.method == 'DELETE':
#         exercise_name.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)
#
#
# class SnippetList(APIView):
#     """
#     List all snippets, or create a new snippet.
#     """
#     def get(self, request, format=None):
#         exercise_name = ExerciseName.objects.all()
#         serializer = ExerciseNameSerializer(exercise_name, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, format=None):
#         serializer = ExerciseNameSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# class SnippetDetail(APIView):
#     """
#     Retrieve, update or delete a snippet instance.
#     """
#     def get_object(self, pk):
#         try:
#             return ExerciseName.objects.get(pk=pk)
#         except ExerciseName.DoesNotExist:
#             raise Http404
#
#     def get(self, request, pk, format=None):
#         exercise_name = self.get_object(pk)
#         serializer = ExerciseNameSerializer(exercise_name)
#         return Response(serializer.data)
#
#     def put(self, request, pk, format=None):
#         exercise_name = self.get_object(pk)
#         serializer = ExerciseNameSerializer(exercise_name, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request, pk, format=None):
#         exercise_name = self.get_object(pk)
#         exercise_name.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)