from django.conf.urls import url
from django.contrib.auth.forms import AuthenticationForm

from django.views.generic import TemplateView
from django.contrib.auth.views import LoginView, LogoutView
from rest_framework.urlpatterns import format_suffix_patterns


# from .forms import OnsAuthForm
from .views import (Index, Home, SessionCreate, SessionDetail, SessionExercise, ExerciseNameCreate, ExerciseNameDetail,
                    ExerciseNameList, SessionExerciseCreate, SessionExerciseUpdate, ExerciseUserDetail)

from . import api

urlpatterns = [
    # ------------------------ API------------------------------------------
    url(r'^snippets/$', api.SnippetList.as_view()),
    url(r'^snippets/(?P<pk>[0-9]+)/$', api.SnippetDetail.as_view()),



    # ------------------------ Normal ---------------------------------------------
    url(r'^logout/$', LogoutView.as_view(template_name='tracker/logout.html', redirect_field_name='tracker:home'), name='logout'),
    url(r'^login/$', LoginView.as_view(template_name='tracker/login.html', ), name='login'),
    url(r'^home/$', Home.as_view(), name='home'),

    # Session URLs
    # Anything within a (?P<label>[]) is sent as an kwarg to the view
    url(r'session/(?P<pk>\d+)/$', SessionDetail.as_view(), name='session'),
    url(r'session/add/$', SessionCreate.as_view(), name='session-add'),
    # Exercise page for specific session - Mash between a detail view and update view
    url(r'session/(?P<pk>\d+)/(?P<exercise_name>[-\w]+)/$', SessionExercise.as_view(),
        name='session_exercise'),
    url(r'session/(?P<pk>\d+)/exercise/(?P<exercise_name>[-\w]+)/(?P<ex_id>\d+)/$', SessionExerciseUpdate.as_view(),
        name='session_exercise_update'),
    # Add new instance of an exercise to a session - redirects to specific page
    url(r'session/(?P<pk>\d+)/(?P<exercise_name>[-\w]+)/add/$', SessionExerciseCreate.as_view(),
        name='session_exercise_create'),

    # Exercise URLs
    url(r'^exercises/$', ExerciseNameList.as_view(), name='exercise_name_list'),
    url(r'exercise_name/add/$', ExerciseNameCreate.as_view(), name='exercise_name_create'),
    # General information about each exercise
    url(r'exercise_name/(?P<slug>[-\w]+)/$', ExerciseNameDetail.as_view(), name='exercise_name'),
    # This url needs to come at the bottom - Shows exercises of one type of exercise name for each user
    url(r'user/(?P<exercise_name>[-\w]+)/$', ExerciseUserDetail.as_view(), name='exercise_user_detail'),

    url(r'^about/$', Index.as_view()),
    url(r'^foundation/$', LoginView.as_view(template_name='tracker/foundation.html')),
    url(r'^/$', Index.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)