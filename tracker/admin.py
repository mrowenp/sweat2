from django.contrib import admin
from .models import Session, ExerciseName, Workout, Reps, Weight, Sets, Exercise, MuscleGroup

# Register your models here.
admin.site.register(Session)
admin.site.register(ExerciseName)
admin.site.register(Workout)
admin.site.register(Reps)
admin.site.register(Weight)
admin.site.register(Sets)
admin.site.register(Exercise)
admin.site.register(MuscleGroup)
