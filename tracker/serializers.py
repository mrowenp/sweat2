from rest_framework import serializers

from .models import ExerciseName


class ExerciseNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExerciseName
        fields = ('id', 'exercise_name', 'slug')
