var ctx = document.getElementById("sessionVolumeChart").getContext('2d');

var session_volume_chart = new Chart(ctx, {
    type: 'line',
    data: volume_data,
    options: {
        scales: {
            xAxes: [{
                type: 'time',
                distribution: 'linear',
                time:{
                    tooltipFormat:'D MMM YYYY',
                    displayFormats:{
                        day: 'D MMM'
                    }
                },

            }]
        }
    }
});