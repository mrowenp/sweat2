var ctx = document.getElementById("sessionVolumeChart").getContext('2d');
var ctx2 = document.getElementById("sessionMaxWeightChart").getContext('2d');

var session_max_weight_chart = new Chart(ctx2, {
    type: 'line',
    data: max_weight_data,
    options: {
        scales: {
            xAxes: [{
                type: 'time',
                distribution: 'linear',
                time:{
                    tooltipFormat:'D MMM YYYY',
                    displayFormats:{
                        day: 'D MMM'
                    }
                },

            }]
        }
    }
});


var session_vol_chart = new Chart(ctx, {
    type: 'line',
    data: volume_data,
    options: {
        scales: {
            xAxes: [{
                type: 'time',
                distribution: 'linear',
                time:{
                    tooltipFormat:'D MMM YYYY',
                    displayFormats:{
                        day: 'D MMM'
                    }
                },

            }]
        }
    }
});
//
//new Chart(document.getElementById("chartjs-0"),
//    {"type":"line",
//    "data":
//        {"labels":["January","February","March","April","May","June","July"],
//         "datasets":
//            [{"label":"My First Dataset",
//            "data":[65,59,80,81,56,55,40],"fill":false,"borderColor":"rgb(75, 192, 192)","lineTension":0.1}]},"options":{}});

//var myChart = new Chart(ctx, {
//    type: 'bar',
//    data: {
//        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
//        datasets: [{
//            label: '# of Votes',
//            data: [12, 19, 3, 5, 2, 3],
//
//            borderWidth: 1
//        }]
//    },
//    options: {
//        scales: {
//            yAxes: [{
//                ticks: {
//                    beginAtZero:true
//                }
//            }]
//        }
//    }
//});
