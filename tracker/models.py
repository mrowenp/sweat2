from django.contrib.auth.models import User
from django.db import models
from django.db.models.functions.base import Concat
from django.urls import reverse

# from .managers import ExerciseManager


# Create your models here.
class Timestamp(models.Model):
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        abstract = True


class Session(Timestamp):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False)

    def __str__(self):
        return str(self.timestamp.strftime('%a %d %B %Y - %H:%M'))

    def get_absolute_url(self):
        return reverse('tracker:session', kwargs={'pk': self.pk})


class Workout(models.Model):
    workout = models.CharField(max_length=50, blank=False, null=False)

    def __str__(self):
        return self.workout


class MuscleGroup(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class ExerciseName(models.Model):
    exercise_name = models.CharField(max_length=60, blank=False, null=False)
    slug = models.SlugField(unique=True)
    equipment_weight = models.IntegerField(default=0, blank=False, null=False)
    weight_multiplier = models.IntegerField(default=1, blank=False, null=False)

    muscle_group = models.ForeignKey(MuscleGroup, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.exercise_name

    def get_absolute_url(self):
        return reverse('tracker:exercise_name', kwargs={'slug': self.slug})


class Reps(models.Model):
    reps = models.IntegerField()

    def __str__(self):
        return str(self.reps)

    class Meta:
        ordering = ['reps']


class Weight(models.Model):
    weight = models.FloatField()

    def __str__(self):
        return str(self.weight)

    class Meta:
        ordering = ['weight']


class Sets(models.Model):
    sets = models.IntegerField()

    def __str__(self):
        return str(self.sets)

    class Meta:
        ordering = ['sets']


class ExerciseManager(models.Manager):
    def get_bench_press(self, user, *args, **kwargs):
        bench_press_obj = ExerciseName.objects.get(slug='bench-press')
        qs = Exercise.objects.filter(exercise=bench_press_obj, user=user)
        return qs

    def get_exercises(self, exercise_name, user, *args, **kwargs):
        exercise_name_obj = ExerciseName.objects.get(slug=exercise_name)
        qs = Exercise.objects.filter(exercise_name_obj)
        return qs

    def get_max_bp(self, user, *args, **kwargs):
        bench_press_obj = ExerciseName.objects.get(slug='bench-press')
        qs = Exercise.objects.filter(exercise=bench_press_obj, user=user)
        qs.annotate(date_weight=Concat('timestamp'))
        return qs

# Annotate each obj with date + weight, order on that, then get the first of each date which should be the largest
# weight as they all have the same date


class Exercise(Timestamp):
    user = models.ForeignKey(User)
    session = models.ForeignKey(Session)
    exercise = models.ForeignKey(ExerciseName)
    weight = models.ForeignKey(Weight, on_delete=models.CASCADE, blank=True, null=True)
    reps = models.ForeignKey(Reps, on_delete=models.CASCADE, blank=True, null=True)
    sets = models.ForeignKey(Sets, on_delete=models.CASCADE, blank=True, null=True)
    comment = models.TextField(blank=True, null=True)

    def __str__(self):
        return str(self.exercise) + ' ' + str(self.weight) + ' x' + str(self.reps) + ' x' + str(self.sets)

    def get_absolute_url(self):
        return reverse('tracker:session_exercise', kwargs={'pk': self.session_id, 'exercise_name': self.exercise.slug})

    def get_volume(self):
        total_weight = (self.weight.weight * self.exercise.weight_multiplier) + self.exercise.equipment_weight
        total_volume = total_weight * self.reps.reps * self.sets.sets
        return total_volume

    objects = models.Manager()
    exercise_manager = ExerciseManager()




