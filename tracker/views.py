from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import F, FloatField, ExpressionWrapper, Sum, Max
from django.urls import reverse
from django.views.generic import FormView, RedirectView, TemplateView, ListView, CreateView, DetailView, RedirectView, UpdateView
from django.views.generic.base import ContextMixin

from .models import Session, Exercise, ExerciseName, Reps, Sets, Weight


# Custom Mixins
class CustomLoginRequiredMixin(LoginRequiredMixin):
    login_url = 'tracker:login'


class CustomContextMixin(object):
    def get_context_data(self, **kwargs):
        context = super(CustomContextMixin, self).get_context_data(**kwargs)

        session_obj = Session.objects.get(id=self.kwargs['pk'])
        context['session'] = session_obj
        context['exercises'] = Exercise.objects.filter(session=session_obj)

        context['exercise_names'] = ExerciseName.objects.all().order_by('muscle_group')

        if 'exercise_name' in self.kwargs:
            exercise_name_obj = ExerciseName.objects.get(slug=self.kwargs['exercise_name'])
            context['exercise_name'] = exercise_name_obj

        print('In mixin')
        print(self.kwargs)
        return context


# Create your views here.
class Index(TemplateView):
    template_name = 'tracker/about.html'


class Home(CustomLoginRequiredMixin, ListView):
    template_name = 'tracker/home.html'
    model = Session
    context_object_name = 'sessions'

    def get_queryset(self):
        print(Exercise.exercise_manager.get_bench_press(user=self.request.user).count())
        return Session.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        # Add more data to be sent to the template
        # Call the base implementation first to get a context
        context = super(Home, self).get_context_data(**kwargs)
        print(get_session_volume_data(self.request))
        context['session_volume_chart'] = get_session_volume_data(self.request)
        return context


def get_session_volume_data(request):
    sessions = Session.objects.filter(user=request.user)
    session_volume_chart = []
    for session in sessions:
        exercises = session.exercise_set.all()
        volume = 0
        for exercise in exercises:
            volume += exercise.get_volume()
        session_volume_chart.append([exercise.timestamp.strftime('%Y-%m-%d'), volume])

    # apply get total volume to each exercise - create own query expression
    # sum each total volume - aggregate
    print(session_volume_chart)
    return session_volume_chart


class SessionDetail(CustomContextMixin, CustomLoginRequiredMixin, DetailView):
    # eg session/1/
    template_name = 'tracker/session.html'
    model = Session
    context_object_name = 'session'

    def get_context_data(self, **kwargs):
        # Add more data to be sent to the template
        # Call the base implementation first to get a context
        context = super(SessionDetail, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the exercises of this session
        # context['exercises'] = Exercise.objects.filter(session_id=self.kwargs['pk'])
        # context['exercise_names'] = ExerciseName.objects.all()

        # Get stats from the session
        # Number of exercises
        session_obj = Session.objects.get(id=self.kwargs['pk'])
        exercise_qs = Exercise.objects.filter(session=session_obj)
        context['exercise_number'] = exercise_qs.distinct('exercise').count()

        # Total weight lifted
        weight_qs = exercise_qs.annotate(
            total_weight=ExpressionWrapper(F('weight__weight') * F('reps__reps') * F('sets__sets'),
                                           output_field=FloatField()))
        for i in weight_qs:
            print(i, i.total_weight)

        print(weight_qs.aggregate(Sum('total_weight')))
        context['total_weight_lifted'] = weight_qs.aggregate(Sum('total_weight'))
        return context


class SessionCreate(CustomLoginRequiredMixin, RedirectView):
    # /session/add - creates a new Session object and redirects to that page

    def get_redirect_url(self, *args, **kwargs):
        # Create new session object
        new_session = Session.objects.create(user=self.request.user)
        url = reverse('tracker:session', kwargs={'pk': new_session.id})
        return url


class SessionExercise(CustomLoginRequiredMixin, ListView):
    # session/(?P<pk>\d+)/exercise/(?P<exercise_name>\w+)/
    # session/1/exercise/dips/
    template_name = 'tracker/session_exercise.html'
    model = Exercise
    context_object_name = 'exercises'

    def get_queryset(self):
        exercise_name = ExerciseName.objects.get(slug=self.kwargs['exercise_name'])
        qs = Exercise.objects.filter(session=self.kwargs['pk'], exercise=exercise_name)
        return qs

    def get_context_data(self, **kwargs):
        # Add more data to be sent to the template
        # Call the base implementation first to get a context
        context = super(SessionExercise, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the exercises of this session
        session_obj = Session.objects.get(id=self.kwargs['pk'])
        context['session'] = session_obj
        exercise_name_obj = ExerciseName.objects.get(slug=self.kwargs['exercise_name'])
        context['exercise_name'] = exercise_name_obj

        return context


class SessionExerciseCreate(CustomLoginRequiredMixin, CreateView):
    # session/(?P<pk>\d+)/(?P<exercise_name>[-\w]+)/add
    template_name = 'tracker/session_exercise_create.html'
    fields = ['weight', 'reps', 'sets', 'comment']
    model = Exercise

    def form_valid(self, form):
        exercise_name = ExerciseName.objects.get(slug=self.kwargs['exercise_name'])
        form.instance.exercise = exercise_name

        session = Session.objects.get(id=self.kwargs['pk'])
        form.instance.session_id = session.id

        # TODO Probably not the best way to get the user, through the session object, but it works for now.
        user = User.objects.get(id=session.user_id)
        form.instance.user = user
        return super(SessionExerciseCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        # Add more data to be sent to the template
        # Call the base implementation first to get a context
        context = super(SessionExerciseCreate, self).get_context_data(**kwargs)

        session = Session.objects.get(id=self.kwargs['pk'])
        context['session'] = session

        exercise_name = ExerciseName.objects.get(slug=self.kwargs['exercise_name'])
        context['exercise_name'] = exercise_name

        user = User.objects.get(id=session.user_id)
        exercise_qs = Exercise.objects.filter(user=user, exercise=exercise_name)
        context['exercise_qs'] = exercise_qs
        return context

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super(SessionExerciseCreate, self).get_initial()
        exercise_name_obj = ExerciseName.objects.get(slug=self.kwargs['exercise_name'])
        one_rep = Reps.objects.get(reps=1)
        one_set = Sets.objects.get(sets=1)

        initial['reps'] = one_rep
        initial['sets'] = one_set

        # Fill weight with same weight as previous exercise
        # TODO need to limit this for just current user, if none => blank
        try:
            last_exercise = Exercise.objects.filter(user=self.request.user, exercise=exercise_name_obj).latest('timestamp')
            initial['weight'] = last_exercise.weight
        except ObjectDoesNotExist:
            pass

        return initial


class SessionExerciseUpdate(CustomLoginRequiredMixin, UpdateView):
    # 'session/(?P<pk>\d+)/exercise/(?P<exercise_name>[-\w]+)/(?P<ex_id>\d+)/$'
    template_name = 'tracker/session_exercise_update.html'
    model = Exercise
    fields = ['weight', 'reps', 'sets', 'comment']
    pk_url_kwarg = 'ex_id'

    def get_context_data(self, **kwargs):
        context = super(SessionExerciseUpdate, self).get_context_data(**kwargs)
        session_id = self.kwargs['pk']
        context['session'] = Session.objects.get(id=session_id)

        exercise_name = ExerciseName.objects.get(slug=self.kwargs['exercise_name'])
        context['exercise_name'] = exercise_name

        exercise_qs = Exercise.objects.filter(session_id=session_id, exercise=exercise_name)
        context['exercise_qs'] = exercise_qs

        ex = Exercise.objects.get(id=self.kwargs['ex_id'])
        print(ex.get_volume())
        return context


class ExerciseNameList(CustomLoginRequiredMixin, ListView):
    template_name = 'tracker/exercise_name_list.html'
    model = ExerciseName
    context_object_name = 'exercises'


# TODO probs need to change this to model exercise and li
class ExerciseNameDetail(CustomLoginRequiredMixin, DetailView):
    # 'exercise_name/(?P<slug>[-\w]+)
    # General information about each exercise
    template_name='tracker/exercise_name.html'
    model = ExerciseName


class ExerciseNameCreate(CustomLoginRequiredMixin, CreateView):
    # exercise_name/add/
    template_name = 'tracker/exercise_name_create.html'
    model = ExerciseName
    fields = ['exercise_name']


class ExerciseUserDetail(CustomLoginRequiredMixin, ListView):
    # '(?P<exercise_name>[-\w]+)/$'
    # Shows exercises of one type of exercise name for each user
    template_name = 'tracker/exercise_user_detail.html'
    model = Exercise
    context_object_name = 'exercises'

    def get_queryset(self):
        exercise_obj = ExerciseName.objects.get(slug=self.kwargs['exercise_name'])
        return Exercise.objects.filter(user=self.request.user, exercise=exercise_obj)

    def get_context_data(self, **kwargs):
        context = super(ExerciseUserDetail, self).get_context_data(**kwargs)
        exercise_name_obj = ExerciseName.objects.get(slug=self.kwargs['exercise_name'])
        context['exercise_name'] = exercise_name_obj

        # Chart data
        # [sessions_date, volume, max_weight]
        sessions = Session.objects.filter(user=self.request.user)
        session_volume_chart = []
        for session in sessions:
            exercises = session.exercise_set.filter(exercise=exercise_name_obj)
            # Max weight
            max_weight = exercises.aggregate(Max('weight__weight'))

            if not exercises:
                continue

            volume = 0
            for exercise in exercises:
                volume += exercise.get_volume()
            session_volume_chart.append([exercise.timestamp.strftime('%Y-%m-%d'), volume, max_weight['weight__weight__max']])
        print(session_volume_chart)
        context['session_volume_chart'] = session_volume_chart

        return context

